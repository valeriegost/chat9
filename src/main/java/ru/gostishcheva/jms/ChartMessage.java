package ru.gostishcheva.jms;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.security.SecureRandom;

@Getter
@Setter
@NoArgsConstructor
public class ChartMessage implements Serializable {

    private String message;
    private String loginFrom;

    @Override
    public String toString(){
        return loginFrom + ": " + message;
    }
}

