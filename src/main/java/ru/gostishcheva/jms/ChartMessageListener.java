package ru.gostishcheva.jms;

import lombok.SneakyThrows;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.sql.SQLOutput;

public class ChartMessageListener implements MessageListener {
    @Override
    @SneakyThrows
    public void onMessage(Message message){
        final boolean checkType = message instanceof ObjectMessage;
        if(!checkType) return;
        final ObjectMessage objectMessage = (ObjectMessage) message;
        System.out.println();
        System.out.println(objectMessage.getObject());
        System.out.println();

    }
}
