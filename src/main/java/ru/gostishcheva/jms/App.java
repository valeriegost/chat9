package ru.gostishcheva.jms;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        System.out.println("**WELCOME TO CHAT**");
        System.out.println("Broadcast -bc | Private - pv");
        System.out.println();
        System.out.println("Enter login:");
        final String login = scanner.nextLine();
        System.out.println("Waiting connecting...");
        System.out.println();
        if (login.isEmpty()) System.exit(-1);
        final Chat chat = new Chat(login);
        chat.init();

        String command = "";
        while (!"exit".equals(command)){
            command = scanner.nextLine();
            if("bc".equals(command)){
                System.out.println("Enter message: ");
                final  String message = scanner.nextLine();
                chat.sendBroadcast(message);
                continue;
            }
            if("pv".equals(command)){
                System.out.println("Enter message: ");
                final  String message = scanner.nextLine();
                System.out.println("Enter login: ");
                final  String loginTo = scanner.nextLine();
                chat.sendPrivate(message, loginTo);
                continue;
            }
        }
    }
}
