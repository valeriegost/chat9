package ru.gostishcheva.jms;

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.Arrays;
import java.util.concurrent.BrokenBarrierException;

@Getter
public class Chat {

    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;
    private static final String BROADCAST = ":BROADCAST:";
    private static final String PRIVATE = ":PRIVATE:";

    private ActiveMQConnectionFactory connectionFactory;

    protected Connection connection;

    private Session session;

    private Destination destinationBroadcast;

    private MessageConsumer consumerBroadcast;

    private MessageProducer producerBroadcast;

    private Destination destinationPrivate;

    private MessageConsumer consumerPrivate;

    private String login;

    public Chat(String login){this.login = login;}

    @SneakyThrows
    public void init(){
        connectionFactory = new ActiveMQConnectionFactory(URL);
        connectionFactory.setTrustedPackages(Arrays.asList("ru.gostishcheva.jms"));
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        final ChartMessageListener listener = new ChartMessageListener();

        destinationBroadcast = session.createTopic(BROADCAST);
        consumerBroadcast = session.createConsumer(destinationBroadcast);
        producerBroadcast = session.createProducer(destinationBroadcast);
        consumerBroadcast.setMessageListener(listener);

        destinationPrivate = session.createQueue(PRIVATE + login);
        consumerPrivate = session.createConsumer(destinationPrivate);
        consumerPrivate.setMessageListener(listener);
    }

    @SneakyThrows
    public void sendBroadcast(final String message){
        if(message.isEmpty()) return;
        final ChartMessage chartMessage = new ChartMessage();
        chartMessage.setMessage(message);
        chartMessage.setLoginFrom(login);
        producerBroadcast.send(session.createObjectMessage(chartMessage));

    }

    @SneakyThrows
    public void sendPrivate(final String message, String loginTo){
        if(message.isEmpty()) return;
        final String destinationName = PRIVATE + loginTo;
        final Destination destination = session.createQueue(destinationName);
        final MessageProducer producerPrivate =session.createProducer(destination);
        final ChartMessage chartMessage = new ChartMessage();
        chartMessage.setMessage(message);
        chartMessage.setLoginFrom(login);
        producerPrivate.send(session.createObjectMessage(chartMessage));

    }


}
